import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyLeagueComponent } from './components/my-league/my-league.component';
import {LeagueRoutingModule} from './league-routing.module';



@NgModule({
  declarations: [MyLeagueComponent],
  imports: [
    CommonModule,
    LeagueRoutingModule,
  ]
})
export class LeagueModule { }
