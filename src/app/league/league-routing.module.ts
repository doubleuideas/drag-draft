import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MyLeagueComponent} from './components/my-league/my-league.component';

const routes: Routes = [
  {path: '', component: MyLeagueComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LeagueRoutingModule {
}
