import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QueenRoutingModule } from './queen-routing.module';
import { QueenComponent } from './components/queen/queen.component';
import {UtilsModule} from '../utils/utils.module';
import {NbButtonModule, NbCardModule} from '@nebular/theme';


@NgModule({
  declarations: [QueenComponent],
  imports: [
    CommonModule,
    QueenRoutingModule,
    UtilsModule,
    NbButtonModule,
    NbCardModule
  ]
})
export class QueenModule { }
