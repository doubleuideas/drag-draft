import {Component, OnInit} from '@angular/core';
import {QueenService} from '../../../services/queen/queen.service';
import {ActivatedRoute} from '@angular/router';
import {map, switchMap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {EventModel} from '../../../models/Event.model';

@Component({
  selector: 'app-queen',
  templateUrl: './queen.component.html',
  styleUrls: ['./queen.component.scss']
})
export class QueenComponent implements OnInit {

  queen$ = this.route.paramMap
    .pipe(
      map(p => p.get('id')),
      switchMap(id => this.queenSrv.getOne(id))
    );

  events$: Observable<EventModel[]> = of([
    { queen: '', createdAt: new Date(Date.now()), id: '12323434', rule: 'Some Rule Name', points: 10 },
    { queen: '', createdAt: new Date(Date.now()), id: '12323434', rule: 'Some Rule Name', points: -10 },
    { queen: '', createdAt: new Date(Date.now()), id: '12323434', rule: 'Some Rule Name', points: 10 },
    { queen: '', createdAt: new Date(Date.now()), id: '12323434', rule: 'Some Rule Name', points: 10 }
  ]);
  // events$ = this.route.paramMap
  //   .pipe(
  //     map(p => p.get('id')),
  //     switchMap(id => this.queenSrv.getQueenEvents(id))
  //   );

  constructor(
    private queenSrv: QueenService,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
  }

}
