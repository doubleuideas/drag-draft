import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {QueenComponent} from './components/queen/queen.component';


const routes: Routes = [
  {path: ':id', component: QueenComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QueenRoutingModule { }
