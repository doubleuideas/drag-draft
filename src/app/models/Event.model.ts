export interface EventModel {
  id: string;
  rule: string;
  queen: string;
  createdAt: Date;
  points: number;
}
