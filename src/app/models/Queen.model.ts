export interface QueenModel {
  id: string;
  name: string;
  img: string;
  bio: string;
  points: number;
  eliminatedOn?: Date;
}
