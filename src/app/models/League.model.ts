export interface LeagueModel {
  id: string;
  draft: {
    currentUser: string;
    round: number;
  };
  owner: string;
  users: string[];
  queenSelections: { [userId: string]: string[] };
}
