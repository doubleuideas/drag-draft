export interface RuleModel {
  id: string;
  name: string;
  points: number;
}
