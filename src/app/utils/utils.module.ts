import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SplitJoinPipePipe} from './pipe/split-join/split-join.pipe';


@NgModule({
  declarations: [SplitJoinPipePipe],
  imports: [
    CommonModule
  ],
  exports: [SplitJoinPipePipe]
})
export class UtilsModule {
}
