import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'splitJoinPipe'
})
export class SplitJoinPipePipe implements PipeTransform {

  transform(text: string, splitOn = '_', joinOn = ' '): string {
    return text.split(splitOn).join(joinOn);
  }

}
