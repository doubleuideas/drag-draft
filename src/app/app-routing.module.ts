import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


const routes: Routes = [
  {path: '', loadChildren: () => import('./home/home.module').then(m => m.HomeModule)},
  {path: 'queen', loadChildren: () => import('./queen/queen.module').then(m => m.QueenModule)},
  {path: 'league', loadChildren: () => import('./league/league.module').then(m => m.LeagueModule)},
  {path: 'draft', loadChildren: () => import('./draft/draft.module').then(m => m.DraftModule)},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
