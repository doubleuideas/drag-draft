import { Component, OnInit } from '@angular/core';
import {QueenService} from '../../../services/queen/queen.service';
import {LeagueService} from '../../../services/league/league.service';
import {RuleService} from '../../../services/rule/rule.service';
import {EventService} from '../../../services/event/event.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    public queenSrv: QueenService,
    public leagueSrv: LeagueService,
    public ruleSrv: RuleService,
    public eventSrv: EventService,
  ) { }

  ngOnInit(): void {
  }

}
