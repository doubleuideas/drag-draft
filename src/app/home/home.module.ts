import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import { HomeComponent } from './components/home/home.component';
import {NbCardModule, NbUserModule} from '@nebular/theme';
import {UtilsModule} from '../utils/utils.module';


@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    AngularFirestoreModule,
    NbCardModule,
    NbUserModule,
    UtilsModule,
  ]
})
export class HomeModule { }
