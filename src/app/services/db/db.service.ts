import {Injectable} from '@angular/core';
import {AngularFirestore, DocumentChangeAction} from '@angular/fire/firestore';
import {map} from 'rxjs/operators';
import {QueenModel} from '../../models/Queen.model';
import {LeagueModel} from '../../models/League.model';
import {RuleModel} from '../../models/Rule.model';
import {EventModel} from '../../models/Event.model';

@Injectable({
  providedIn: 'root'
})
export class DbService {

  private season = 'season_12';

  queens$ = this.afs.collection(`/seasons/${this.season}/queens`, ref => ref.orderBy('points', 'desc')).snapshotChanges()
    .pipe(
      map(docs => this.addIdToDocs<QueenModel>(docs))
    );

  leagues$ = this.afs.collection(`/seasons/${this.season}/leagues`).snapshotChanges()
    .pipe(
      map(docs => this.addIdToDocs<LeagueModel>(docs))
    );

  rules$ = this.afs.collection(`/seasons/${this.season}/rules`).snapshotChanges()
    .pipe(
      map(docs => this.addIdToDocs<RuleModel>(docs))
    );

  events$ = this.afs.collection(`/seasons/${this.season}/events`).snapshotChanges()
    .pipe(
      map(docs => this.addIdToDocs<EventModel>(docs))
    );

  constructor(private afs: AngularFirestore) {
  }


  private addIdToDocs<T>(docs: DocumentChangeAction<unknown>[]): T[] {
    return docs.map(d => {
      const doc = d.payload.doc.data() as T;
      return {
        id: d.payload.doc.id,
        ...doc,
      };
    });
  }
}
