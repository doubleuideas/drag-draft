import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {DbService} from '../db/db.service';
import {Observable} from 'rxjs';
import {QueenModel} from '../../models/Queen.model';
import {EventService} from '../event/event.service';
import {EventModel} from '../../models/Event.model';

@Injectable({
  providedIn: 'root'
})
export class QueenService {

  queens$ = this.db.queens$;

  constructor(
    private db: DbService,
    private eventSrv: EventService,
  ) {
  }

  getOne(id: string): Observable<QueenModel> {
    return this.queens$
      .pipe(
        map(queens => queens.find(q => q.id === id))
      );
  }

  getQueenEvents(queenId: string): Observable<EventModel[]> {
    return this.eventSrv.events$
      .pipe(
        map(events => events.filter(e => e.queen === queenId))
      );
  }
}
