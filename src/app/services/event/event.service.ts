import { Injectable } from '@angular/core';
import {DbService} from '../db/db.service';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  events$ = this.db.events$;

  constructor(
    private db: DbService
  ) { }
}
