import {Injectable} from '@angular/core';
import {DbService} from '../db/db.service';

@Injectable({
  providedIn: 'root'
})
export class RuleService {

  rules$ = this.db.rules$;

  constructor(
    private db: DbService
  ) {
  }
}
