import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DraftRoutingModule } from './draft-routing.module';
import { DrafterComponent } from './components/drafter/drafter.component';
import { DraftSelectorComponent } from './components/draft-selector/draft-selector.component';


@NgModule({
  declarations: [DrafterComponent, DraftSelectorComponent],
  imports: [
    CommonModule,
    DraftRoutingModule
  ]
})
export class DraftModule { }
