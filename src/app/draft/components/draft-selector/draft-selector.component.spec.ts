import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DraftSelectorComponent } from './draft-selector.component';

describe('DraftSelectorComponent', () => {
  let component: DraftSelectorComponent;
  let fixture: ComponentFixture<DraftSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DraftSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DraftSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
