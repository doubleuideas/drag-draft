import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DrafterComponent} from './components/drafter/drafter.component';
import {DraftSelectorComponent} from './components/draft-selector/draft-selector.component';


const routes: Routes = [
  {path: '', component: DraftSelectorComponent},
  {path: ':leagueId', component: DrafterComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DraftRoutingModule {
}
