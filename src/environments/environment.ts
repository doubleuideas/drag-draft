// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyC8JCgfgkcdhpqu50ASTbwT-E925894p5w',
    authDomain: 'drag-draft-test.firebaseapp.com',
    databaseURL: 'https://drag-draft-test.firebaseio.com',
    projectId: 'drag-draft-test',
    storageBucket: 'drag-draft-test.appspot.com',
    messagingSenderId: '480489585525',
    appId: '1:480489585525:web:507df81c1b22db10'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
